# Repositorio completo de infraestructura

Necesitas instalar FLUX-CLI para lanzar los comandos de instalación en el cluster.

https://toolkit.fluxcd.io/guides/installation/

Una vez instalado despliega todo lo necesario de flux en el cluster.

```
export GITLAB_TOKEN=xxxxxxxx

flux bootstrap gitlab --components-extra=image-reflector-controller,image-automation-controller --owner=juanjmerono --repository=flux-k8s-infra --branch=master --path=clusters/local-cluster --personal
```

Puedes añadir la gestión de infraestructura de todos los cluster en el mismo repositorio:

```
flux bootstrap gitlab --components-extra=image-reflector-controller,image-automation-controller --owner=juanjmerono --repository=flux-k8s-infra --branch=master --path=clusters/<new-cluster-name> --personal
```

## Mononitoring

Incluye la infraestructura requerida con el fichero gotk-monitoring.yaml y haz push al repositorio para que, tras reconciliar, dispongas de graphana listo para explorar el panel de control de flux.

```
kubectl -n flux-system port-forward svc/grafana 3000:3000
```

## Selaed Secrets

Incluye la infraestructura requerida con el fichero gotk-sealed-secrets.yaml y haz push al repositorio para que, tras reconciliar, dispongas de la posibilidad de publicar tus secretos cifrados en el repositorio.

Debes instalar KUBESEAL-CLI para poder obtener la clave pública única para el cluster con la que debes cifrar los secretos.

```
kubeseal --fetch-cert --controller-name=sealed-secrets --controller-namespace=flux-system > clusters/local-cluster/flux-system/pub-sealed-secrets.pem
```

## Add image automation update

Incluye la infraestructura requerida con el fichero gotk-image-automation.yaml y haz push al repositorio para que, tras reconciliar, dispongas de la posibilidad de actualizar el repositorio de infraestructura automáticamente cuando se publique una nueva imagen en el registro docker.

## Add deployments

Ahora queda ya el proceso de ir incluyendo diferentes despliegues. Todos los despliegues se describen en el mismo repositorio que debes incluir con el fichero gotk-deployents-source.yaml.

Se podría tener cada descripción de infraestructura separada en un repositorio diferente, pero por simplicidad estará todo en un único repositorio. 

Dentro de la carpeta flux-kustomizations se creará una carpeta por cada aplicativo que contendrá una kustomization para cada entorno en el que se despliegue.

### Cifrar secretos

Es normal que necesites secretos y debes incluirlos en la infraestructura y cifrarlos con kubeseal.

#### Credenciales para el registro docker

```
kubectl create secret generic regcred --from-file=.dockerconfigjson=<path/to/.docker/config.json> --type=kubernetes.io/dockerconfigjson --dry-run=client -o yaml > secret.yaml

kubeseal --scope=cluster-wide --format=yaml --cert=clusters/local-cluster/flux-system/pub-sealed-secrets.pem < secret.yaml > sealed-secret.yaml
```

#### Credenciales del repositorio

```
flux create secret git repocred --url=https://gitlab.com/u/repo     --username=user --password=<gitlab-token> --export > secret.yaml

kubeseal --scope=cluster-wide --format=yaml --cert=clusters/local-cluster/flux-system/pub-sealed-secrets.pem < secret.yaml > sealed-secret.yaml
```

#### Otro tipo de secretos

```
kubectl create secret generic secret-name --from-literal=msg=MensajitoSecreto --dry-run=client -o yaml > secret.yaml

kubeseal --scope=cluster-wide --format=yaml --cert=clusters/local-cluster/flux-system/pub-sealed-secrets.pem < secret.yaml > sealed-secret.yaml
```

